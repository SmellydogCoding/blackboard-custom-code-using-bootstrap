# Custom HTML for Blackboard Content That Supports Uploading Custom Code

## Dependencies

### Less CSS

A standalone version of the [LESS](http://lesscss.org/) command-line compiler that will run on Windows with no other dependencies.

Consists of a standalone version of [Node.js](http://nodejs.org/) and the required less.js files/dependencies. 

### BootStrap 4.5

**[https://getbootstrap.com/](https://getbootstrap.com/)**

## Usage

Clone project

If you edit bootstrap.css, run LESS CSS to create a new bootstrap-iso CSS File:

`.\lessc --strict-math=on  prefix.less bootstrap-iso-4.5.css`

upload the bootstrap-iso-4.5.css file to a CDN

See the ClassOneInstaller folder for examples of how to import the bootstrap-iso file and usage